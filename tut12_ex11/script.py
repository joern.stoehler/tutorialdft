from fleurpy import FleurFolder
from fleurpy import computer
import os
import numpy
import math
import itertools
from scipy import constants
from pprint import pprint
from matplotlib import pyplot as plt

inp_file = "fccFe.inp"
stages = {'start': False,
          'mod': False,
          'run': False,
          'plot': True}

base = computer.get_base(__file__)

# create base inp.xml and sym.out
start = FleurFolder(path=os.path.join(base, "start"))
if stages['start']:
    start.copy_from([inp_file], base)
    start.run_inpgen(inp_file=inp_file, cmd_inpgen=computer.cmd_inpgen, args=["-noco"])


def setup_folder(folder, x, y):
    folder.copy_from(files=["inp.xml", "sym.out"], src_dir=start)

    # max runs to guarantee convergence (hopefully)
    folder.set_xml_xpath_attribs(xpath=".//scfLoop", attrib="itmax", value=90,
                                 xml_file="inp.xml", unique=True)
    folder.set_xml_xpath_attribs(xpath=".//scfLoop", attrib="minDistance", value=5e-4,
                                 xml_file="inp.xml", unique=True)
    folder.set_xml_xpath_attribs(xpath=".//scfLoop", attrib="maxIterBroyd", value=15,
                                 xml_file="inp.xml", unique=True)

    folder.set_kpointmesh(nx=7, ny=7, nz=7)

    folder.set_xml_xpath_attribs(xpath='.//atomGroup[@species="Fe-1"]/nocoParams', attrib="beta", value="Pi/2.0",
                                 xml_file="inp.xml", unique=True)
    folder.set_xml_xpath_text(xpath=".//nocoParams/qss", value="%f %f %f" % (x, y, 0.0),
                              xml_file="inp.xml", unique=True)


def run_folder(folder):
    folder.run_fleur(cmd_fleur=computer.cmd_fleur, env={"OMP_NUM_THREADS": "8"})


def plot_folder(folder: FleurFolder, x, y, data):
    iteration_count = folder.out_xml_get_iteration_count()
    last = folder.out_xml_get_last_iteration_node()

    energy = folder.get_xml_xpath_attribs(xpath=".//totalEnergy",
                                          attrib="value", root=last, unique=True)

    d_charge = folder.get_xml_xpath_attribs(xpath=".//densityConvergence/overallChargeDensity",
                                            attrib="distance", root=last, unique=True)

    data.append(f"{x} {y} {energy} {iteration_count} {d_charge}")


qss_x = numpy.linspace(0.0, 0.5, 11)
qss_y = numpy.linspace(0.0, 0.5, 11)

folder_list = [
    (FleurFolder(path=os.path.join(base, "calculation", ("%.2f_%.2f" % (x, y)).replace(".", ""))),
     x, y)
    for x, y in zip(qss_x, qss_y)
]

# vary parameters and store results
if stages['mod']:
    for folder, x, y in folder_list:
        setup_folder(folder, x, y)

if stages['run']:
    # create a bash script for running
    script = []
    for folder, x, y in folder_list:
        script.append(folder.path)
        folder.run_fleur(cmd_fleur=computer.cmd_fleur, env={"OMP_NUM_THREADS": "6"}, clean=False)

    base_folder = FleurFolder(path=os.path.join(base))
    base_folder.save_file("folders.txt", "\n".join(script))


if stages['plot']:
    plot_data = ["# qss_x qss_y energy iteration_count d_charge"]
    for folder, x, y in folder_list:
        plot_folder(folder, x, y, plot_data)

    base_folder = FleurFolder(path=os.path.join(base))
    base_folder.save_file("qXYCoord_energy.txt", "\n".join(plot_data))
