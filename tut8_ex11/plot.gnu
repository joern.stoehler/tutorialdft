set parametric
set isosamples 50
set urange [0:2.0*pi]
set vrange [0:pi]

a=K_1 # plug in K_1 here
b=K_2 # plug in K_2 here

k(v) = sin(v)*sin(v)*a
l(v) = sin(v)*sin(v)*sin(v)*sin(v)*b

fx(v,u) = sin(v)*cos(u) * (k(v)+l(v))
fy(v,u) = sin(v)*sin(u) * (k(v)+l(v))
fz(v) = cos(v) * (k(v)+l(v))
splot fx(v,u),fy(v,u),fz(v)