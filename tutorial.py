#!/bin/env python3
import sys

if len(sys.argv) >= 2:
    exercise = sys.argv[1]
    if exercise == "tut7_ex11":
        import tut7_ex11.script
    elif exercise == "tut7_ex21":
        import tut7_ex21.script
    elif exercise == "tut8_ex11":
        import tut8_ex11.script
    elif exercise == "tut8_ex21":
        import tut8_ex21.script
    elif exercise == "tut8_ex22":
        import tut8_ex22.script

print("usage: python3 tutorial.py <exercise_folder>")