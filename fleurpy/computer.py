import socket, os

hostname = socket.gethostname()

known_computers = [
    {
        'hostname': "matrioshka",
        # 'base': "/home/joern/University/MasterPhysik/DFT/TutorialDFT/",
        'cmd_fleur': "/home/joern/University/MasterPhysik/DFT/localexec/build.matrioshka.mpi/fleur_MPI",
        'cmd_inpgen': "/home/joern/University/MasterPhysik/DFT/localexec/build.matrioshka.mpi/inpgen",
    },
    {
        'hostname': "Ontology",
        # 'base': "/home/joern/University/MasterPhysik/DFT/TutorialDFT/",
        'cmd_fleur': "/home/joern/University/MasterPhysik/DFT/localexec/build.mpi/fleur_MPI",
        'cmd_inpgen': "/home/joern/University/MasterPhysik/DFT/localexec/build.mpi/inpgen"
    },
    {
        'hostname': "login18-1.hpc.itc.rwth-aachen.de",
        # 'base': "/home/vo580608/DFT/tutorialdft/",
        'cmd_fleur': "/home/vo580608/DFT/build/fleur_MPI",
        'cmd_inpgen': "/home/vo580608/DFT/build/inpgen"
    }
]

# detect local machine
computer = next(c for c in known_computers if c['hostname'] == hostname)
# base = computer['base']
cmd_fleur = computer['cmd_fleur']
cmd_inpgen = computer['cmd_inpgen']


def get_base(python_file):
    return os.path.dirname(os.path.abspath(python_file))
