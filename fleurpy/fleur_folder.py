import subprocess, os.path, shutil, contextlib
from lxml import etree


class FleurFolder:
    def __init__(self, path=""):
        self.path = path
        os.makedirs(path, exist_ok=True)

    def copy_to(self, files, dst_dir):
        if isinstance(dst_dir, FleurFolder):
            dst_dir = dst_dir.path

        for f in files:
            shutil.copyfile(os.path.join(self.path, f), os.path.join(dst_dir, f))

    def copy_from(self, files, src_dir, alias=None):
        if alias is None:
            alias = files
        if isinstance(src_dir, FleurFolder):
            src_dir = src_dir.path
        for f, a in zip(files, alias):
            shutil.copyfile(os.path.join(src_dir, f), os.path.join(self.path, a))

    def delete_files(self, files):
        with contextlib.suppress(FileNotFoundError):
            for f in files:
                os.remove(os.path.join(self.path, f))

    def run_fleur(self, cmd_fleur="fleur", clean=True):
        if clean:
            self.delete_files(["cdn.hdf", "mixing_history"])
        subprocess.call([cmd_fleur], cwd=self.path)

    def run_fleur_scf(self, cmd_fleur="fleur", clean=True):
        if clean:
            self.delete_files(["cdn.hdf", "mixing_history"])
        converged = False
        while not converged:
            pass

    def run_gnuplot(self, inp_file="plot.gnu", variables=[], out_file="plot.png", size="400,300",
                    cmd_gnuplot="gnuplot", custom_save="png"):
        self.delete_files([out_file])

        cmd = [cmd_gnuplot]

        if custom_save == "png":
            cmd.append("-e")
            cmd.append(f"set terminal png size {size};")
        elif custom_save is not None:
            return  # unsupported extension

        for v in variables:
            cmd.append("-e")
            cmd.append(v)

        cmd.append(inp_file)

        if out_file is not None:
            with open(os.path.join(self.path, out_file), "w+") as f:
                print(cmd)
                subprocess.call(cmd, cwd=self.path, stdout=f)
        else:
            subprocess.call(cmd, cwd=self.path)

    def run_ps2pdf(self, inp_file="bands.ps", out_file="bands.pdf"):
        subprocess.call(["ps2pdf", inp_file, out_file], cwd=self.path)

    def save_file(self, out_file="out.txt", src_str=""):
        with open(os.path.join(self.path, out_file), 'w+') as f:
            f.write(src_str)

    def run_inpgen(self, inp_file="inp", cmd_inpgen="inpgen"):
        self.delete_files(["inp.xml"])
        with open(os.path.join(self.path, inp_file), "r") as f:
            subprocess.call([cmd_inpgen], cwd=self.path, stdin=f)

    def get_xml_xpath_nodes(self, xpath, xml_file=None, non_empty=True, unique=False, root=None):
        if xml_file is not None:
            tree = etree.parse(os.path.join(self.path, xml_file))
            if root is None:
                root = tree.getroot()

        nodes = root.xpath(xpath)
        if not non_empty and not nodes:
            return RuntimeError
        if unique and len(nodes) != 1:
            return RuntimeError

        if unique:
            return nodes[0]
        else:
            return nodes

    def get_xml_xpath_attribs(self, xpath, attrib, xml_file=None, non_empty=True, unique=False, root=None):
        return self.map_xml_xpath_attribs(xpath, attrib, lambda v: v, xml_file, non_empty, unique, root)

    def set_xml_xpath_attribs(self, xpath, attrib, value, xml_file=None, non_empty=True, unique=False, root=None):
        return self.map_xml_xpath_attribs(xpath, attrib, lambda v: value, xml_file, non_empty, unique, root)

    def scale_xml_xpath_attribs(self, xpath, attrib, scale, xml_file=None, non_empty=True, unique=False, root=None):
        return self.map_xml_xpath_attribs(xpath, attrib, lambda v: f"{v}*{scale}", xml_file, non_empty, unique, root)

    def map_xml_xpath_attribs(self, xpath, attrib, fn, xml_file=None, non_empty=True, unique=False, root=None):
        if xml_file is not None:
            tree = etree.parse(os.path.join(self.path, xml_file))
            if root is None:
                root = tree.getroot()

        nodes = root.xpath(xpath)
        if not non_empty and not nodes:
            return RuntimeError
        if unique and len(nodes) != 1:
            return RuntimeError

        change = False
        new_values = []
        for n in nodes:
            v = n.get(attrib)
            nv = str(fn(v))
            n.set(attrib, nv)
            new_values.append(nv)
            if nv != v:
                change = True

        if xml_file is not None and change:
            with open(os.path.join(self.path, xml_file), 'wb') as f:
                f.write(etree.tostring(tree))

        if unique:
            return new_values[0]
        else:
            return new_values

    def out_xml_get_iteration_count(self):
        return max([int(i) for i in self.get_xml_xpath_attribs(xpath=".//iteration", attrib="overallNumber",
                                                               xml_file="out.xml", unique=False)])

    def out_xml_get_last_iteration_node(self):
        iteration_count = self.out_xml_get_iteration_count()
        return self.get_xml_xpath_nodes(xpath=f".//iteration[@overallNumber={iteration_count}]",
                                        xml_file="out.xml", unique=True)

    def set_kpath(self, kpath: list, kcount=201, gamma="F",
                  xml_file="inp.xml"):
        tree = etree.parse(os.path.join(self.path, xml_file))
        root = tree.getroot()

        xpath = ".//altKPointSet/kPointCount"
        node = root.xpath(xpath)[0]

        node.set("count", str(kcount))
        node.set("gamma", gamma)

        for label, x, y, z in kpath:
            etree.SubElement(node, "specialPoint", attrib={"name": label}) \
                .text = f"{x} {y} {z}"

        with open(os.path.join(self.path, xml_file), 'wb') as f:
            f.write(etree.tostring(tree))


def __repr__(self):
    return self.path.__repr__()
