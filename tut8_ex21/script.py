from fleurpy import FleurFolder
from fleurpy import computer
import os
import numpy
import math
from scipy import constants
from pprint import pprint
from matplotlib import pyplot as plt

inp_file = "fccNi.inp"
stages = {'start': False,
          'mod': False,
          'run': False,
          'plot': True}

base = computer.get_base(__file__)

# create base inp.xml and sym.out
start = FleurFolder(path=os.path.join(base, "start"))
if stages['start']:
    start.copy_from([inp_file], base)
    start.run_inpgen(inp_file=inp_file, cmd_inpgen=computer.cmd_inpgen)


def setup_folder(folder, theta="0.0", phi="Pi/2.0"):
    folder.copy_from(files=["inp.xml", "sym.out"], src_dir=start)

    # max runs to guarantee convergence (hopefully)
    folder.set_xml_xpath_attribs(xpath=".//scfLoop", attrib="itmax", value=50,
                                 xml_file="inp.xml", unique=True)

    # spin angles
    folder.set_xml_xpath_attribs(xpath=".//calculationSetup/soc", attrib="theta", value=theta,
                                 xml_file="inp.xml", unique=True)
    folder.set_xml_xpath_attribs(xpath=".//calculationSetup/soc", attrib="phi", value=phi,
                                 xml_file="inp.xml", unique=True)
    folder.set_xml_xpath_attribs(xpath=".//calculationSetup/soc", attrib="l_soc", value="T",
                                 xml_file="inp.xml", unique=True)

    pass


# vary parameters and store results
data_columns = []

# E = K0 * 1 + K1 * (a1^2 * a2^2 + ...) + K2 * (a1^2 * a2^2 * a3^2) + ...
# a1 = cos(theta)
# a2 = sin(theta)*cos(phi)
# a3 = sin(theta)*sin(phi)
# [(a1,a2,a3)] = [(1,0,0), (1,1,0)/sqrt(2), (1,1,1)/sqrt(3)]

setup_theta = [0.0, math.acos(2.0 ** -.5), math.acos(3.0 ** -0.5)]
setup_phi = [0.0, 0.0, math.acos(2.0 ** -.5)]

for index in [0, 1, 2]:
    folder = FleurFolder(path=os.path.join(base, "calculation", ("%d" % index).replace(".", "_")))

    # modify inp.xml
    if stages['mod']:
        setup_folder(folder, theta=str(setup_theta[index]), phi=str(setup_phi[index]))

    # run fleur until self-consistency is reached
    if stages['run']:
        folder.run_fleur(cmd_fleur=computer.cmd_fleur)

    # extract data
    if stages['plot']:
        iteration_count = folder.out_xml_get_iteration_count()
        last = folder.out_xml_get_last_iteration_node()

        energy = folder.get_xml_xpath_attribs(xpath=".//totalEnergy",
                                              attrib="value", root=last, unique=True)

        d_charge = folder.get_xml_xpath_attribs(xpath=".//densityConvergence/overallChargeDensity",
                                                attrib="distance", root=last, unique=True)

        o_theta = folder.get_xml_xpath_attribs(xpath=".//calculationSetup/soc", attrib="theta",
                                               xml_file="out.xml", unique=True)
        o_phi = folder.get_xml_xpath_attribs(xpath=".//calculationSetup/soc", attrib="phi",
                                             xml_file="out.xml", unique=True)

        data_columns.append({
            'folder': folder, 'theta': o_theta, 'phi': o_phi,
            'iteration_count': iteration_count,
            'energy': energy,
            'd_charge': d_charge,
        })

# pprint(data_columns)

# plot data
if stages['plot']:
    folder = FleurFolder(path=base)
    data_rows = {}
    # transpose columns into rows
    for key in ['theta', 'phi', 'energy', 'iteration_count', 'd_charge']:
        data_rows[key] = numpy.array([float(i[key]) for i in data_columns])

    folder.save_file(out_file="energy.txt",
                     src_str='\n'.join(str(e) for e in data_rows['energy']))

    # solve E = K0 + K1*(a1^2*a2^2+...) + K2*a1^2*a2^2*a3^2 + ...
    t = numpy.array([data_rows['theta'][i] for i in [0, 1, 2]])
    p = numpy.array([data_rows['phi'][i] for i in [0, 1, 2]])
    E = numpy.array([data_rows['energy'][i] for i in [0, 1, 2]])
    a1 = numpy.cos(t)
    a2 = numpy.sin(t) * numpy.cos(p)
    a3 = numpy.sin(t) * numpy.sin(p)

    K = numpy.linalg.solve(
        numpy.array([
            [1, 1, 1],
            a1 ** 2 * a2 ** 2 + a2 ** 2 * a3 ** 2 + a3 ** 2 * a1 ** 2,
            a1 ** 2 * a2 ** 2 * a3 ** 2
        ]).transpose(), E)

    folder.run_gnuplot(inp_file="plot.gnu", out_file="plot.png", size="800,600",
                       variables=[f"K_0={K[0]}", f"K_1={K[1]}", f"K_2={K[2]}"])

    vol = float(data_columns[0]['folder'].get_xml_xpath_attribs(xpath=".//volumes", attrib="unitCell",
                                                                xml_file="out.xml", unique=True))  # random folder
    vol_SI = vol * constants.value('Bohr radius') ** 3
    # convert K/vol from eV/a0^3 to J/m^3
    K_SI = K * constants.value('electron volt') / vol_SI

    folder.save_file(out_file="fit.txt",
                     src_str=f"# K0 K1 K2 [J/m^3] vol [m^3] K0 K1 K2 [eV]\n"
                             f"{K_SI[0]} {K_SI[1]} {K_SI[2]} {vol_SI} {K[0]} {K[1]} {K[2]}")
