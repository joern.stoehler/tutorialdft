set parametric
set isosamples 50
set urange [0:2.0*pi]
set vrange [0:pi]

a1(v,u)=cos(v)
a2(v,u)=sin(v)*cos(u)
a3(v,u)=sin(v)*sin(u)

E1(v,u)=K_1*(a1(v,u)**2 * a2(v,u)**2 + a2(v,u)**2 * a3(v,u)**2 + a3(v,u)**2 * a1(v,u)**2)
E2(v,u)=K_2*(a1(v,u)**2 * a2(v,u)**2 * a3(v,u)**2)

fx(v,u) = sin(v)*cos(u) * (E1(v,u)+E2(v,u))
fy(v,u) = sin(v)*sin(u) * (E1(v,u)+E2(v,u))
fz(v,u) = cos(v) * (E1(v,u)+E2(v,u))
splot fx(v,u),fy(v,u),fz(v,u)