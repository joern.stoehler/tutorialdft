set terminal postscript enhanced color "Times-Roman" 20
set xlabel ""
set ylabel "E - E_F (eV)"
set nokey
set title "hP GaN"
set arrow from  0.00000, -9.0 to  0.00000,  5.0 nohead
set arrow from  0.60177, -9.0 to  0.60177,  5.0 nohead
set arrow from  0.94920, -9.0 to  0.94920,  5.0 nohead
set arrow from  1.64406, -9.0 to  1.64406,  5.0 nohead
set arrow from  1.96444, -9.0 to  1.96444,  5.0 nohead
set arrow from  2.56621, -9.0 to  2.56621,  5.0 nohead
set arrow from  2.91364, -9.0 to  2.91364,  5.0 nohead
set arrow from  3.60851, -9.0 to  3.60851,  5.0 nohead
set arrow from  0.00000, 0.0 to  3.60851, 0.0 nohead lt 3
set xtics (" "  0.00000, \
           "M"  0.60177, \
           "K"  0.94920, \
           " "  1.64406, \
           "A"  1.96444, \
           "L"  2.56621, \
           "H"  2.91364, \
           "A"  3.60851  )
set label "G" at   0.00000, -9.65 center font "Symbol,20"
set label "G" at   1.64406, -9.65 center font "Symbol,20"
set ytics -8,2,4
plot [0:  3.60852] [-9:5] \
"bands.1" using 1:($2+0.00)  w p pt  7 ps 0.5
