# TutorialDFT

(Possibly incorrect) solutions to the 2019 DFT Tutorial.

### Content

This repository contains only the scripts and final plots, not the calculated data.

### Usage

Modify the variables `base, cmd_fleur, cmd_inpgen` in `fleurpy/computer.py` to reflect your local computer.

Modify the variable `stages = {...}` in e.g. `tut7_ex21/script.py` to enable and disable various calculation steps. 
Setting everything to `True` reruns the whole exercise and builds the output.

Because of python packaging structure you need to start the scripts from the project folder e.g.

`python3 tutorial.py tut7_ex21`

### Requirements

Python:
* numpy
* matplotlib
* lxml

Other:
* fleur

---

## older tutorials: tut5-tut7

Requires `module load ipython3` on CLAIX for the python packages `numpy`, `matplotlib`.

The python-script starts bash-shells which then call the programs `fleur` and `inpgen`, so make sure they are available after loading `~/.bashrc`.

Usage: `ipython3 script.py` or import to manually call various stages of the exercise.

To reduce initial download size, consider a shallow clone:   
`git clone --depth 1 https://git.rwth-aachen.de/rwth5/tutorialdft.git`.

Status of Exercises

* Tutorial 5: 20.11.2019 , [Density of states](https://www.flapw.de/site/DFT-2019-tut/densityOfStates/)
  * Exercise 1.1: slightly old python code
  * Exercise 2.1: done
* Tutoiral 6: 04.12.2019, [Force relaxations and thin films](https://www.flapw.de/site/DFT-2019-tut/forcesAndFilms/)
  * Exercise 1.1: TBD
  * Exercise 2.1: TBD
