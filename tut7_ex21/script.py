from fleurpy import FleurFolder
from fleurpy import computer
import os
import numpy
from pprint import pprint
from matplotlib import pyplot as plt

exercise = "tut7_ex21"
inp_file = "bccCr.inp"
stages = {'start': True,
          'mod': {'FM': True, 'AFM': True},
          'run': {'FM': True, 'AFM': True},
          'plot': {'FM': True, 'AFM': True}}

base = computer.get_base(__file__)

# create base inp.xml and sym.out
start = FleurFolder(path=os.path.join(base, "start"))
if stages['start']:
    start.copy_from([inp_file], base)
    start.run_inpgen(inp_file=inp_file, cmd_inpgen=computer.cmd_inpgen)

# vary parameters and store results
data_columns = []
for magnet in ["FM", "AFM"]:
    for scale in [0.95, 0.96, 0.97, 0.98, 0.99, 1.00, 1.01, 1.02, 1.03]:
        folder = FleurFolder(path=os.path.join(base, magnet, ("%.3f" % scale).replace(".", "_")))

        # modify inp.xml
        if stages['mod'][magnet]:
            folder.copy_from(files=["inp.xml", "sym.out"], src_dir=start)

            # switch magnetization of Cr-2
            if magnet == 'AFM':
                folder.scale_xml_xpath_attribs(xpath=".//atomSpecies/species[@name='Cr-2']", attrib="magMom",
                                               scale='(-1)',
                                               xml_file="inp.xml", unique=True)
            # scale mtSpheres
            folder.scale_xml_xpath_attribs(xpath=".//mtSphere", attrib="radius", scale=0.95,
                                           xml_file="inp.xml", unique=False)

            # scale lattice constant
            folder.scale_xml_xpath_attribs(xpath=".//bulkLattice", attrib="scale", scale=scale,
                                           xml_file="inp.xml", unique=True)

            # max runs to guarantee convergence (hopefully)
            folder.set_xml_xpath_attribs(xpath=".//scfLoop", attrib="itmax", value=50,
                                         xml_file="inp.xml", unique=True)

            # higher nkpt
            folder.set_xml_xpath_attribs(xpath=".//bzIntegration/kPointCount", attrib="count", value=54,
                                         xml_file="inp.xml", unique=True)

        # run fleur until self-consistency is reached
        if stages['run'][magnet]:
            folder.run_fleur(cmd_fleur=computer.cmd_fleur)

        # extract data
        if stages['plot'][magnet]:
            iteration_count = folder.out_xml_get_iteration_count()
            last = folder.out_xml_get_last_iteration_node()

            spin1 = folder.get_xml_xpath_attribs(xpath=".//allElectronCharges/spinDependentCharge[@spin=1]",
                                                 attrib="total", root=last, unique=True)
            spin2 = folder.get_xml_xpath_attribs(xpath=".//allElectronCharges/spinDependentCharge[@spin=2]",
                                                 attrib="total", root=last, unique=True)

            mag1 = folder.get_xml_xpath_attribs(xpath=".//magneticMoment[@atomType=1]",
                                                attrib="moment", root=last, unique=True)
            mag2 = folder.get_xml_xpath_attribs(xpath=".//magneticMoment[@atomType=2]",
                                                attrib="moment", root=last, unique=True)

            energy = folder.get_xml_xpath_attribs(xpath=".//totalEnergy",
                                                  attrib="value", root=last, unique=True)

            d_spin1 = folder.get_xml_xpath_attribs(xpath=".//densityConvergence/chargeDensity[@spin=1]",
                                                   attrib="distance", root=last, unique=True)
            d_spin2 = folder.get_xml_xpath_attribs(xpath=".//densityConvergence/chargeDensity[@spin=2]",
                                                   attrib="distance", root=last, unique=True)
            d_charge = folder.get_xml_xpath_attribs(xpath=".//densityConvergence/overallChargeDensity",
                                                    attrib="distance", root=last, unique=True)
            d_spin = folder.get_xml_xpath_attribs(xpath=".//densityConvergence/spinDensity",
                                                  attrib="distance", root=last, unique=True)

            data_columns.append({
                'folder': folder, 'scale': scale, 'magnet': magnet,
                'iteration_count': iteration_count,
                'spin1': spin1, 'spin2': spin2, 'mag1': mag1, 'mag2': mag2,
                'energy': energy,
                'd_spin1': d_spin1, 'd_spin2': d_spin2, 'd_charge': d_charge, 'd_spin': d_spin,
            })

# pprint(data_columns)

# plot data
if stages['plot']['FM'] and stages['plot']['AFM']:
    data_rows = {'FM': {}, 'AFM': {}}
    # transpose columns into rows
    for magnet in ['FM', 'AFM']:
        for key in ['scale', 'energy', 'spin1', 'spin2', 'mag1', 'mag2', 'iteration_count', 'd_charge', 'd_spin']:
            data_rows[magnet][key] = numpy.array([float(i[key]) for i in data_columns if i['magnet'] == magnet])

    # plot graph
    plt.figure(0)
    fig, ax1 = plt.subplots()
    ax1.set_xlabel("lattice constant [5.50 a_0]")
    ax1.set_ylabel("totalEnergy [Htr]")

    ax1.plot(data_rows['FM']['scale'], data_rows['FM']['energy'],
             '-o', label="FM energy")
    ax1.plot(data_rows['AFM']['scale'], data_rows['AFM']['energy'],
             '-o', label="AFM energy")

    ax2 = ax1.twinx()
    ax2.set_ylabel("magnetisation [muBohr]")

    ax2.plot(data_rows['FM']['scale'], (data_rows['FM']['spin1'] - data_rows['FM']['spin2']) / 2.0,
             ':1', label="FM spin")
    ax2.plot(data_rows['AFM']['scale'], (data_rows['AFM']['spin1'] - data_rows['AFM']['spin2']) / 2.0,
             ':2', label="AFM spin")

    ax2.plot(data_rows['FM']['scale'], data_rows['FM']['mag1'],
             ':3', label="FM mag1")
    ax2.plot(data_rows['AFM']['scale'], data_rows['AFM']['mag1'],
             ':4', label="AFM mag1")

    fig.legend()
    fig.tight_layout()
    fig.savefig(os.path.join(base, "plot_data.png"))

    # plot convergece
    plt.figure(1)
    fig, ax1 = plt.subplots()
    ax1.set_xlabel("lattice constant [5.50 a_0]")
    ax1.set_ylabel("charge distance [me/bohr^3]")
    ax1.set_yscale('log')

    ax1.plot(data_rows['FM']['scale'], data_rows['FM']['d_charge'],
             '--1', label="FM charge density")
    ax1.plot(data_rows['AFM']['scale'], data_rows['AFM']['d_charge'],
             '--2', label="AFM charge density")
    ax1.plot(data_rows['FM']['scale'], data_rows['FM']['d_spin'],
             ':3', label="FM spin density")
    ax1.plot(data_rows['AFM']['scale'], data_rows['AFM']['d_spin'],
             ':4', label="AFM spin density")

    ax2 = ax1.twinx()
    ax2.set_ylabel("iteration count [1]")
    ax2.plot(data_rows['FM']['scale'], data_rows['FM']['iteration_count'],
             '-o', label="FM iterations")
    ax2.plot(data_rows['AFM']['scale'], data_rows['AFM']['iteration_count'],
             '-o', label="AFM iterations")

    fig.legend()
    fig.tight_layout()
    fig.savefig(os.path.join(base, "plot_convergence.png"))
