#!/usr/bin/python3

import os, subprocess, sys, contextlib
import shutil
from xml.etree import ElementTree as et
import numpy as np
import matplotlib.pyplot as plt

cmd_fleur="fleur"
cmd_inpgen="inpgen"

# atomic units
a0 = 1.0

#from inp.xml
ntype = 1 #number of atom types
LMAX = 4

#params
exercise_title="Tut6/Ex1.1"
_index = [0,1,2,3,4,5]
_tmpdir=[f"t-{i}" for i in _index]
inpdir="inp"
inpfile="inpMoS2.txt"
outdir="out"

#measurements
fermi=-0.1149406587 #Htr


def make_input_file(i):
	tmpdir = _tmpdir[i]

	shutil.copyfile(f"{inpdir}/inp.xml", f"{tmpdir}/inp.xml")
	shutil.copyfile(f"{inpdir}/sym.out", f"{tmpdir}/sym.out")

	tree = et.parse(f"{tmpdir}/inp.xml")

	#base
	xml_set(tree, "calculationSetup/scfLoop",'itmax', 50)
	scale_mtspheres(tree, 0.95)

	#self
	if i == 0:
		pass

	#band
	if i == 1:
		xml_set(tree,"output",'band', 'T')
		xml_set(tree,"cutoffs",'numbands', 25)

	#force
	if i == 2:
		xml_set(tree,"geometryOptimization",'l_f', 'T')
		xml_set(tree,"geometryOptimization",'forcemix',  'BFGS')

	tree.write(f"{tmpdir}/inp.xml")

def main():
	plot_special()

#### general functions

def run_fleur(i):
	tmpdir = _tmpdir[i]
	cmd = f"cd {tmpdir}; {cmd_fleur}"
	subprocess.call(['/bin/bash', '-i', '-c', cmd])

def copy_dos(srci, dsti):
	files = ["cdn.hdf", "DOS.1", "banddos.hdf", "bands.1", "out", "out.xml"]
	for file in files:
		shutil.copyfile(f"{_tmpdir[srci]}/{file}", f"{_tmpdir[dsti]}/{file}")

def make_dirs():
	os.makedirs(inpdir, exist_ok=True)
	os.makedirs(outdir, exist_ok=True)
	for i in _index:
		os.makedirs(_tmpdir[i], exist_ok=True)

def remove_dirs():
	with contextlib.suppress(FileNotFoundError):
		shutil.rmtree(inpdir)
		shutil.rmtree(outdir)
		for i in _index:
			shutil.rmtree(_tmpdir[i])

def run_inpgen():
	shutil.copyfile(f"{inpfile}", f"{inpdir}/{inpfile}")
	cmd = f"cd {inpdir}; {cmd_inpgen} < {inpfile} "
	subprocess.call(['/bin/bash', '-i', '-c', cmd])

def extract_energy(indices):
	if indices == None: indices = _index

	table=""
	for j in range(len(indices)):
		tmpdir = _tmpdir[indices[j]]

		tree = et.parse(f"{tmpdir}/out.xml")
		energy = xml_getlast(tree, "totalEnergy", 'value') #unit: Htr
		table += f"{energy}\n"

	with open(f'{outdir}/totalEnergies.txt', 'w') as outfile:
		outfile.write(table)


def plot_special(indices):
	if indices == None: indices = _index

	table=""
	
	dlist = []
	elist = []
	flist = []
	
	for j in range(len(indices)):
		tmpdir = _tmpdir[indices[j]]
		
		displacement = (1.000 + 0.01 * indices[j]) / 2.000

		tree = et.parse(f"{tmpdir}/out.xml")
		energy = xml_getlast(tree, "totalEnergy", 'value') #unit: Htr
		dlist.append(displacement)
		elist.append(energy)
		flist.append(force)
	
	plt.tight_layout()
	#plt.get_current_fig_manager().window.showMaximized()
	plt.savefig(f"plot-all.png")
	plt.show()

def plot_bands(indices):
	if indices == None: indices = _index
	os.makedirs(outdir, exist_ok=True)

	for j in range(len(indices)):
		tmpdir = _tmpdir[indices[j]]

		tree = et.parse(f"{tmpdir}/out.xml")
		bandgap = 	xml_get(tree, "bandgap", 'value')
		bandgap_u = xml_get(tree, "bandgap", 'units')
		fermi = 	xml_get(tree, "FermiEnergy", 'value')
		fermi_u = 	xml_get(tree, "FermiEnergy", 'units')

		print(f"Bandgap = {bandgap} {bandgap_u}")
		print(f"Fermi = {fermi} {fermi_u}")

		cmd = f"cd {tmpdir} ; \
				gnuplot < band.gnu > bands.ps ; \
				ps2pdf bands.ps ; \
				mv bands.pdf ../{outdir}/bands_{tmpdir}.pdf"

		subprocess.call(['/bin/bash', '-i', '-c', cmd])

def plot_density(indices):
	if indices == None: indices = _index
	os.makedirs(outdir, exist_ok=True)

	ROWS=1
	COLUMNS=1
	plt.rcParams["figure.figsize"] = (12,8)
	fig, splt = plt.subplots(ROWS,COLUMNS, sharex="col", sharey="row")
	#splt[-1,-1].axis('off')

	for j in range(len(indices)):
		tmpdir = _tmpdir[indices[j]]

		row = int(j%ROWS)
		col = int(j/ROWS)

		ax = splt#[row][col]

#e,totdos,interstitial,vac1,vac2,(at(i),i=1,ntype),((q(l,i),l=1,LMAX),i=1,ntype)
		data = np.loadtxt(f"{tmpdir}/DOS.1", skiprows=0, usecols=None, unpack=True)
		e = data[0] #unit: eV
		totdos = data[1]
		interstitial = data[2]
		vac1 = data[3]
		vac2 = data[4]
		at = [data[5+i] for i in range(0, ntype)]
		#q(i,l)
		q = [[data[5+ntype+ l + i*LMAX] for l in range(0,LMAX)] for i in range(0,ntype)]

		print(f"total columns {1 + 5+ntype+(LMAX-1)+(ntype-1)*LMAX}")

		sdos = q[0][0]
		pdos = q[0][1]
		ddos = q[0][2]

		ax.plot(e, totdos, '-', label='totdos')
		ax.plot(e, sdos, '-', label='s')
		ax.plot(e, pdos, ':', label='p')
		ax.plot(e, ddos, ':', label='d')

#		ax.set_title(f'')
		if row==ROWS-1:
			ax.set_xlabel("Energy [eV]")
		if col==0:
			ax.set_ylabel("DensityOfStates [eV^-1]")
		if row==0 and col==0:
			ax.legend()


	fig.suptitle(exercise_title)

	plt.tight_layout()
	#plt.get_current_fig_manager().window.showMaximized()
	plt.savefig(f"{outdir}/plot-dos.png")
	plt.show()

def scale_mtspheres(tree, factor):
	for node in tree.getroot().findall(".//mtSphere"):
		r=node.get("radius")
		node.set("radius", f"{r}*{factor}")

###### Wrapper and Library Utility Functions

def xml_set(tree, nodename, attribname, value):
	tree.getroot().findall(f".//{nodename}")[0].set(attribname, str(value))

def xml_get(tree, nodename, attribname):
	return tree.getroot().findall(f".//{nodename}")[0].get(attribname)

def xml_get_last(tree, nodename, attribname):
	return tree.getroot().findall(f".//{nodename}")[-1].get(attribname)

def xml_add_child(tree, nodename,  childname, text, **kwargs):
	parent = tree.getroot().findall(f".//{nodename}")[0]
	et.SubElement(parent, childname, **kwargs).text=text

###### Main Function

if __name__=="__main__":
	main()
