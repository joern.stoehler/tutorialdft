# TutorialDFT

## Tutorial 7 (11.12.19) - Simple magnetic systems

### Exercise 1.1 - hcp Co

#### Convergence
Convergence was reached except for anti-ferromagnetic (AFM) Cobalt at scale=0.98.

![Convergence Plot](plot_convergence.png)

#### Data

The ground-state energy is lower for ferromagnetic (FM) Cobalt, which confirms with expectation. 
FM has a larger optimal lattice constant than AFM.

The magnetization of FM is dominated by the muffin-tin-spheres, significant (oom. 1 muBohr), 
and nearly constant in the lattice constant.

AFM Cobalt has no total magnetization (as expected by symmetry).

The magnetisation of the muffin-tin-spheres (FM mag1 and thus FM spin; AFM mag1) declines under compression. 
For AFM it reaches 0 somewhere between scale=0.97..0.99, which roughly coincides with the 
optimal lattice constant for AFM Cobalt.
 
![Data Plot](plot_data.png)