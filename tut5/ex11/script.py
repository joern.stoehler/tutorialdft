#!/usr/bin/python3

import os, subprocess, sys
import shutil
from xml.etree import ElementTree as et
import numpy as np
import matplotlib.pyplot as plt

cmd_fleur="fleur"
cmd_inpgen="inpgen"

# atomic units
a0 = 1.0

#from inp.xml
ntype = 1 #number of atom types
LMAX = 4

#params
_index = range(0,5)
_outdir=[f"t-{i}"	for i in _index]
_inpdir=["inp" 		for i in _index]

_sigma = [0.015, 0.005, 0.0015, 0.0015, 0.0015]
_kpoint= [    8,     8,      8,     23,     47]



def make_input_files():
	for i in _index:
		outdir = _outdir[i]
		inpdir = _inpdir[i]
		sigma  = _sigma[i]
		kpoint = _kpoint[i]

		os.makedirs(outdir, exist_ok=True)
		shutil.copyfile(f"{inpdir}/inp.xml", f"{outdir}/inp.xml")
		shutil.copyfile(f"{inpdir}/sym.out", f"{outdir}/sym.out")

		tree = et.parse(f"{outdir}/inp.xml")

		tree.getroot().findall("./calculationSetup/scfLoop")[0].set('itmax', str(40))

		tree.getroot().findall(".//kPointMesh")[0].set('nx', str(kpoint))
		tree.getroot().findall(".//kPointMesh")[0].set('ny', str(kpoint))
		tree.getroot().findall(".//kPointMesh")[0].set('nz', str(kpoint))
		tree.getroot().findall(".//kPointMesh")[0].set('gamma', 'F')

		#densityOfState
		if True:
			tree.getroot().findall("./output")[0].set('dos', 'T')
			tree.getroot().findall(".//densityOfStates")[0].set('ndir', '-1')
			tree.getroot().findall(".//densityOfStates")[0].set('sigma', str(sigma))

		#band
		if False:
			tree.getroot().findall("./output")[0].set('band', 'T')
			tree.getroot().findall(".//altKPointSet/kPointCount")[0].set('count', str(300))
			tree.getroot().findall("./calculationSetup/cutoffs")[0].set('numbands', str(25))

		tree.write(f"{outdir}/inp.xml")

def run_fleur():
	for i in _index:
		outdir = _outdir[i]
		cmd = f"cd {outdir}; {cmd_fleur}"
		subprocess.call(['/bin/bash', '-i', '-c', cmd])


def clean_fleur_output():
	for i in _index:
		outdir = _outdir[i]
		cmd = f"rm {outdir}/cdn*" #out out.xml usage.json juDFT_times.json FleurInputSchema.xsd
		subprocess.call(['/bin/bash', '-i', '-c', cmd])

def extract_energy():
	table=""
	for i in _index:
		outdir = _outdir[i]

		tree = et.parse(f"{outdir}/out.xml")
		energy = tree.getroot().findall(".//totalEnergy")[-1].get('value') #unit: Htr
		table += f"{energy}\n"
	with open('totalEnergies.txt', 'w') as outfile:
		outfile.write(table)

def plot_bands():
	for i in _index:
		outdir = _outdir[i]

		tree = et.parse(f"{outdir}/out.xml")
		bandgap = tree.getroot().findall(".//bandgap")[-1].get('value')
		bandgap_u = tree.getroot().findall(".//bandgap")[-1].get('units')

		print(f"Bandgap = {bandgap} {bandgap_u}")

		cmd = f"cd {outdir}; gnuplot < band.gnu > bands.ps ; ps2pdf bands.ps ;"
		subprocess.call(['/bin/bash', '-i', '-c', cmd])

def plot_density():
	plt.rcParams["figure.figsize"] = (12,8)
	fig, splt = plt.subplots(3,2, sharex="col", sharey="row")
	splt[-1,-1].axis('off')

	for index in _index:
		outdir = _outdir[index]
		sigma  = _sigma[index]
		kpoint = _kpoint[index]

		row = int(index%3)
		col = int(index/3)

		ax = splt[row][col]

#e,totdos,interstitial,vac1,vac2,(at(i),i=1,ntype),((q(l,i),l=1,LMAX),i=1,ntype)
		data = np.loadtxt(f"{outdir}/DOS.1", skiprows=0, usecols=None, unpack=True)
		e = data[0] #unit: eV
		totdos = data[1]
		interstitial = data[2]
		vac1 = data[3]
		vac2 = data[4]
		at = [data[5+i] for i in range(0, ntype)]
		#q(i,l)
		q = [[data[5+ntype+ l + i*LMAX] for l in range(0,LMAX)] for i in range(0,ntype)]

		sdos = q[0][0]
		pdos = q[0][1]

		ax.plot(e, totdos, '-', label='totdos')
		ax.plot(e, sdos, '-', label='s')
		ax.plot(e, pdos, '-', label='p')

		ax.set_title(f'({index+1}) kpt={kpoint} sigma={sigma}')
		if row==2:
			ax.set_xlabel("Energy [eV]")
		if col==0:
			ax.set_ylabel("DensityOfStates [eV^-1]")
		if row==0 and col==0:
			ax.legend()


	fig.suptitle("Ex 5-1.1")

	plt.tight_layout()
	#plt.get_current_fig_manager().window.showMaximized()
	plt.savefig(f"plot-dos.png")
	plt.show()

	pass

if __name__=="__main__":
	if 'run' in sys.argv:
		clean_fleur_output()
		make_input_files()
		run_fleur()
		plot_density()

	if 'clean' in sys.argv:
		clean_fleur_output()
	if 'make' in sys.argv:
		make_input_files()
	if 'fleur' in sys.argv:
		run_fleur()
	if 'energy' in sys.argv:
		extract_energy()
	if 'bands' in sys.argv:
		plot_bands()
	if 'plot' in sys.argv:
		plot_density()
