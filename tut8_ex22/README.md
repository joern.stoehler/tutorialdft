# TutorialDFT

## Tutorial 8 (18.12.19) - Spin-orbit coupling

### Exercise 2.2 - Rashba effect in BiTeCl

#### Convergence

Convergence was reached everywhere.

![Convergence Plot](plot_convergence.png)

#### Data

The ground-state energy is lower for anti-ferromagnetic (AFM) Chromium, which confirms with expectation.

The total magnetization of AFM is negligible as expected by symmetry. 
The magnetization in muffin-tin-sphere (AFM mag1) is significant (~oom. 1 muBohr) and declines under compression.
Other than for AFM Cobalt (exercise 1.1) it remains significant even at the optimal lattice constant.

The magnetization of FM is negligible in the muffin-tin-spheres (FM mag1) and in total (FM spin).
 
![Data Plot](plot_data.png)