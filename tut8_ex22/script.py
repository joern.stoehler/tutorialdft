from fleurpy import FleurFolder
from fleurpy import computer
import os
import numpy
import math
from scipy import constants
from pprint import pprint
from matplotlib import pyplot as plt

inp_file = "hpBiTeCl.inp"
stages = {'start': False,
          'mod': [False, False, False, False],
          'run': [False, False, False, False],
          'plot': True}

base = computer.get_base(__file__)

# create base inp.xml and sym.out
start = FleurFolder(path=os.path.join(base, "start"))
if stages['start']:
    start.copy_from([inp_file], base)
    start.run_inpgen(inp_file=inp_file, cmd_inpgen=computer.cmd_inpgen)


def setup_folder(folder, soc_l="T", bands=False, parent=None):
    folder.copy_from(files=["inp.xml", "sym.out"], src_dir=start)

    # max runs to guarantee convergence (hopefully)
    folder.set_xml_xpath_attribs(xpath=".//scfLoop", attrib="itmax", value=50,
                                 xml_file="inp.xml", unique=True)

    # spin angles
    folder.set_xml_xpath_attribs(xpath=".//calculationSetup/soc", attrib="l_soc", value=soc_l,
                                 xml_file="inp.xml", unique=True)

    # bands
    folder.set_xml_xpath_attribs(xpath=".//cutoffs", attrib="numbands", value=150,
                                 xml_file="inp.xml", unique=True)
    folder.set_kpath(kpath=[("K", "1./3.", "1./3.", "0."),
                            ("g", "0.", "0.", "0."),
                            ("M", "0.", "1./2.", "0.")],
                     kcount=201)

    # band calculation
    if bands:
        # sc results
        folder.copy_from(files=["cdn.hdf"], src_dir=parent)
        # enable band calculation
        folder.set_xml_xpath_attribs(xpath=".//output", attrib="band", value="T",
                                     xml_file="inp.xml", unique=True)


# vary parameters and store results
data_columns = []

folder0 = FleurFolder(path=os.path.join(base, "calculation", "0"))
folder1 = FleurFolder(path=os.path.join(base, "calculation", "1"))
folder2 = FleurFolder(path=os.path.join(base, "calculation", "2"))
folder3 = FleurFolder(path=os.path.join(base, "calculation", "3"))

if stages['mod'][0]:
    setup_folder(folder0, soc_l="F", bands=False, parent=None)
if stages['run'][0]:
    folder0.run_fleur(cmd_fleur=computer.cmd_fleur)

if stages['mod'][1]:
    setup_folder(folder1, soc_l="F", bands=True, parent=folder0)
if stages['run'][1]:
    folder1.run_fleur(cmd_fleur=computer.cmd_fleur)

if stages['mod'][2]:
    setup_folder(folder2, soc_l="T", bands=False, parent=None)
if stages['run'][2]:
    folder2.run_fleur(cmd_fleur=computer.cmd_fleur)

if stages['mod'][3]:
    setup_folder(folder3, soc_l="T", bands=True, parent=folder2)
if stages['run'][3]:
    folder3.run_fleur(cmd_fleur=computer.cmd_fleur)

# plot data
if stages['plot']:
    for folder in [folder1, folder3]:
        folder.run_gnuplot(inp_file="band.gnu", out_file="bands.ps", custom_save=None,
                           variables=[])
        folder.run_ps2pdf(inp_file="bands.ps", out_file="bands.pdf")

    folderbase = FleurFolder(path=base)
    folderbase.copy_from(files=["bands.pdf"], alias=["bands1.pdf"], src_dir=folder1)
    folderbase.copy_from(files=["bands.pdf"], alias=["bands3.pdf"], src_dir=folder3)
