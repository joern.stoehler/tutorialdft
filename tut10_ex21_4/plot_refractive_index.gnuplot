
# complex n (complex epsilon_r)

nc(ec)=ec**0.5
c(r,i)={1,0}*r+{0,1}*i

plot "dielecR" using ($1*27.211):(real(nc(c($2,$3)))), \
"dielecR" using ($1*27.211):(imag(nc(c($2,$3))))
