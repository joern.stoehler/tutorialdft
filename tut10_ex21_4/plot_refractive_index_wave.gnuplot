
# complex n (complex epsilon_r)

nc(ec)=ec**0.5
c(r,i)={1,0}*r+{0,1}*i

h=4.135667696e-15 #eV*s
c=299792458 #m/s

# wavelength (energy in Hartree)
L(E)=h*c/(27.211*E)*1e+9

plot "dielecR" every ::30 using (L($1)):(real(nc(c($2,$3)))), \
"dielecR" every ::30 using (L($1)):(imag(nc(c($2,$3))))
